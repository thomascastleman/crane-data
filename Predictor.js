
const moment = require('moment');

class Predictor {

  // construct a predictor & generate prediction
  constructor(data) {
    this.data = JSON.parse(JSON.stringify(data)); // copy data
    this.predictionDate = this.parseDate(); // what day are we predicting for (always the day after the most recent date recorded)
    this.prediction = this.predict(); // generate prediction vector
  }

  // train the model and generate its prediction
  predict() {
    this.colorTypes = []; // list of all distinct types of colors--parsed from raw data

    // parse dates to moment objects for date math
    for (let i = 0; i < this.data.length; i++) {
      this.data[i].realDate = moment(this.data[i].date, 'M/D/YY');

      // ensure date validity
      if (!this.data[i].realDate.isValid()) throw new Error('Failed to parse all dates properly.');

      // add new color type to list
      if (!this.colorTypes.includes(this.data[i].color)) {
        this.colorTypes.push(this.data[i].color);
      }
    }

    this.data = this.data.reverse();  // reverse order so we don't do everything backwards. I don't need help predicting the past.

    /*  partition the data into its consecutive chunks--I'll only take
      ngrams from sections that are consecutive */
    const partitions = this.partitionDataConsecutively();

    const lastPartition = partitions[partitions.length - 1];  // get the final (most recent, in time) partition

    this.nToNgrams = {}; // map from N to all ngrams with length N

    for (let i = 0; i < partitions.length; i++) {
      /* max USABLE ngram within this partition (we match against the last partition, so there's
        no point in looking at sequences that are longer than that final partition) */
      let maxN = Math.min(partitions[i].length - 1, lastPartition.length);

      // for each of those possible ngrams
      for (let n = 1; n <= maxN; n++) {
        // extract all the grams of this length that exist in this subsection, and add them into nToNgrams
        this.extractAllGramsOfN(n, partitions[i]);
      }
    }

    const allNs = Object.keys(this.nToNgrams);  // list of values of N for which ngrams were found
    const matches = []; // list of objects encoding ngrams that exactly matched onto subsection of final partition
    let softMaxDenom = 0; // denominator sum for softmax function

    for (let i = 0; i < allNs.length; i++) {
      let n = allNs[i];   // what n are we looking at
      let map = this.nToNgrams[n];  // get the mapping from gram of colors to the following color, for this n

      let grams = Object.keys(map); // get just the grams so we can iterate over them

      // for each recorded gram of length n
      for (let j = 0; j < grams.length; j++) {
        let g = grams[j].split(',');  // convert from comma-separated string back into array

        /* if I were to line up this gram with the end of the final partition--however
          much they overlap--does it match exactly? If so, we can use it for prediction. */
        if (this.matchesFinalSubsection(lastPartition, g)) {
          // record what N, and what are the options for following this ngram
          matches.push({
            n: n,
            outcomes: map[g]
          });
          softMaxDenom += Math.exp(n); // add e^n to sum of all the e^n values
          break;  // only one from each value of N, so don't continue
        }
      }
    }

    if (matches.length == 0 || softMaxDenom == 0) return;  // can't predict

    // for each matching ngram
    for (let i = 0; i < matches.length; i++) {
      // convert from list of colors to the distribution of frequencies of those colors
      matches[i].distribution = this.computeDistribution(matches[i].outcomes);

      matches[i].coeff = Math.exp(matches[i].n) / softMaxDenom; // compute softmax coefficient
    }

    const result = {};

    for (let i = 0; i < this.colorTypes.length; i++) {
      // to compute the ith value of the final distribution, sum the weighted values
      // from the ith value of the distribution each individual match
      let p = matches.reduce((acc, cur) => (cur.distribution[i] * cur.coeff) + acc, 0);

      // map color name to overall probability of occurrence 
      result[this.colorTypes[i]] = p;
    }

    return result;
  }

  // compute a vector representing the distribution of each color of this.colorTypes occuring in the outcomes list
  // the ith value in this distribution is the P(this.colorTypes[i]) occurs as an outcome of this ngram
  computeDistribution(outcomes) {
    if (outcomes.length == 0) throw new Error("Can't compute distribution with no outcomes!");

    let colorToFreq = {};

    // count the frequency of each color outcome
    for (let i = 0; i < outcomes.length; i++) {
      if (!colorToFreq[outcomes[i]]) colorToFreq[outcomes[i]] = 0;
      colorToFreq[outcomes[i]]++;
    }

    const dist = []; // distribution is vector of length: number of color types

    for (let i = 0; i < this.colorTypes.length; i++) {
      let c = this.colorTypes[i];
      let f = colorToFreq[c] || 0;

      // normalize frequency, add to distribution
      dist.push(f / outcomes.length);
    }

    return dist;
  }

  // check that some list b matches some subsection of the end of list a
  matchesFinalSubsection(a, b) {
    if (b.length > a.length) throw new Error("Cannot match large array onto end of smoler array");

    // get the part of a that is of same length as b
    let final = a.slice(-1 * b.length);

    // check array equality
    for (let i = 0; i < final.length; i++) {
      if (final[i] != b[i]) return false;
    }

    return true;
  }

  // add all ngrams of length n from a partition p into global nToNgrams
  extractAllGramsOfN(n, p) {
    if (n >= p.length) throw new Error("Cannot compute ngrams for N greater than or equal to partition size");

    // if no entries exist for this N yet, start new map
    if (!this.nToNgrams[n]) {
      this.nToNgrams[n] = {};
    }

    // get the map for specifically the N we're looking at
    let map = this.nToNgrams[n];

    for (let i = 0; i < p.length - n; i++) {
      let gram = p.slice(i, i + n);   // get the N preceding terms
      let follow = p[i + n];      // get the term that follows

      // need to update: gram -> [follow, ...]
      if (!map[gram]) {
        map[gram] = [follow]; // if never before seen, start new follow entry list
      } else {
        map[gram].push(follow); // if this has been seen, add new follow entry
      }
    }
  }

  // determine if an ngram consists solely of consecutive dates (no gaps)  
  isConsecutiveSequence(seq) {
    if (seq.length == 1) return true;   // sequence of length 1 is consecutive
    if (seq.length == 0) throw new Error("Cannot compute consecutivity on empty sequence");

    for (let i = 0; i < seq.length - 1; i++) {
      // next day must be one away from current day
      let consecutive = seq[i + 1].realDate.diff(seq[i].realDate, 'days') == 1;
      if (!consecutive) return false;
    }

    return true;
  }

  // partition the data into groups that are consecutive internally
  partitionDataConsecutively() {
    if (this.data.length == 0) throw new Error("Cannot partition with no data");
    if (this.data.length == 1) return [this.data];

    const partitions = [];  // holder for all subsections
    let current = []; // current subsection of the partition

    for (let i = 0; i < this.data.length; i++) {
      // add just color of data to current partition--don't care about date anymore since we're partitioning to guarantee consecutivity
      current.push(this.data[i].color);

      // if we're at the end of the stream or if the next day is non-consecutive, push to partition
      if (i == this.data.length - 1 || this.data[i + 1].realDate.diff(this.data[i].realDate, 'days') > 1) {
        partitions.push(current);
        current = []; // start a new subsection
      }
    }

    return partitions;
  }

  // parse the date of the most recent color reporting, to determine when we are predicting for (the next day)
  parseDate() {
    if (this.data.length == 0) return;
    return moment(this.data[0].date, 'M/D/YY')  // parse date of most recent data point
            .add(1, 'day')                      // get the next day (consecutive)
            .format('M/D/YYYY');
  }
}

module.exports = Predictor;