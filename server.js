
/*
  server.js: Main server file
*/

const express           = require('express');
const app               = express();
const mustacheExpress   = require('mustache-express');
const bodyParser        = require('body-parser');
const fs                = require('fs');
const csv               = require('fast-csv');
const urlify            = require('seo-urlify');
const Predictor         = require('./Predictor.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.engine('html', mustacheExpress());
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views'));

const port = 8787;      // server port
const rawData = [];     // crane color data from CSV
const renderData = [];  // data to send to mustache for page rendering
let p;                  // predictor instance

const fileStream = fs.createReadStream(`${__dirname}/data.csv`);
const parser = csv.parse();

fileStream
    .pipe(parser)
    .on('error', error => console.error(error))
    .on('readable', () => {
      for (let row = parser.read(); row; row = parser.read()) {
        if (row[0] !== 'Date' && row[1] !== 'Color' && row[2] !== 'Notes') {
          // add just date & color as raw data
          rawData.push({
            date: row[0],
            color: row[1],
            notes: row[2]
          });

          // add date, color & parsed color class (hyphen-separated) for render
          renderData.push({
            date: row[0],
            color: row[1],
            colorClass: urlify(row[1]),
            notes: row[2]
          });
        }
      }
    })
    .on('end', (rowCount) => {
      p = new Predictor(rawData); // create a predictor

      // start server
      var server = app.listen(port, () => {
        console.log(`Tower crane server listening on port ${server.address().port}`);
      });
    });

// send raw color data
app.get('/api/data', (req, res) => {
  res.send({
    err: !rawData ? "Failed to find any crane data." : null,
    data: rawData
  });
});

// send raw prediction data
app.get('/api/prediction', (req, res) => {
  res.send({
    err: !p.prediction ? "Failed to generate prediction." : null, 
    prediction: p.prediction,
    predictionDate: p.predictionDate
  });
});

app.get('/', (req, res) => {
  res.render('home.html', { renderData });
});

app.get('/predict', (req, res) => {
  if (!p.prediction) {
    res.render('predict.html');
  } else {

    const colors = p.colorTypes;
    const prediction = [];

    for (let i = 0; i < colors.length; i++) {
      prediction.push({
        color: colors[i],
        probability: p.prediction[colors[i]].toFixed(2),
        percent: (p.prediction[colors[i]] * 100).toFixed(2),
        colorClass: urlify(colors[i])
      });
    }

    res.render('predict.html', { 
      prediction,
      predictionDate: p.predictionDate,
      hasPrediction: true
    });
  }
});

// unhandled routes redirect to home
app.get('*', (req, res) => { res.redirect('/'); });